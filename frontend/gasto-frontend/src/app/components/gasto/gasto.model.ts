export interface Gasto {
    codigo?: number
    nomePessoa: string
    descricaoGasto: string
    dataHoraGasto: Date
    valorGasto: number
    tagGasto: string
}