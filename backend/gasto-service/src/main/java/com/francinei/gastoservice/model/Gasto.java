package com.francinei.gastoservice.model;

import java.io.Serializable;
import java.math.BigDecimal;
import java.time.LocalDateTime;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;

import lombok.Data;
import lombok.NoArgsConstructor;

@Entity(name = "TB_GASTO")
@Data
@NoArgsConstructor
public class Gasto implements Serializable {

	private static final long serialVersionUID = -1304240184869386125L;

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "ID_GASTO")
	private Long codigo;

	@Column(name = "DS_NOME_PESSOA")
	private String nomePessoa;

	@Column(name = "DS_DESCRICAO")
	private String descricaoGasto;

	@Column(name = "DT_DATA_HORA")
	private LocalDateTime dataHoraGasto;

	@Column(name = "VL_VALOR")
	private BigDecimal valorGasto;

	@Column(name = "DS_TAG")
	private String tagGasto;

}
