package com.francinei.gastoservice.service.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.francinei.gastoservice.model.Gasto;
import com.francinei.gastoservice.repository.GastoRepository;
import com.francinei.gastoservice.service.GastoService;

@Service
public class GastoServiceImpl implements GastoService {

	@Autowired
	private GastoRepository gastoRepository;

	@Override
	public void salvarGasto(Gasto gasto) {
		gastoRepository.save(gasto);
	}

	@Override
	public Iterable<Gasto> retornarGastos() {
		return gastoRepository.findAll();
	}

	@Override
	public Gasto retornarGastoPorCodigo(Long codigo) {
		return gastoRepository.findByCodigo(codigo).orElseGet(Gasto::new);
	}

}