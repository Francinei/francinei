package com.francinei.gastoservice.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.francinei.gastoservice.model.Gasto;
import com.francinei.gastoservice.service.GastoService;

@RestController
@RequestMapping("/gastos")
public class GastoController {

	@Autowired
	private GastoService gastoService;

	@GetMapping(path = "/retornarGastos")
	public Iterable<Gasto> retornarGastos() {
		return gastoService.retornarGastos();
	}

	@GetMapping(path = "/retornarGastoPorCodigo/{codigo}")
	public Gasto retornarGastoPorCodigo(@PathVariable("codigo") Long codigo) {
		return gastoService.retornarGastoPorCodigo(codigo);
	}

	@CrossOrigin
	@PostMapping(path = "/salvar")
	public void addMember(@RequestBody Gasto gasto) {
		gastoService.salvarGasto(gasto);
	}

}
