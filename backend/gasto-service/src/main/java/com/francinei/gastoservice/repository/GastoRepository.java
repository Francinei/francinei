package com.francinei.gastoservice.repository;

import java.util.Optional;

import org.springframework.data.jpa.repository.JpaRepository;

import com.francinei.gastoservice.model.Gasto;

public interface GastoRepository extends JpaRepository<Gasto, Long> {

	Optional<Gasto> findByCodigo(Long codigo);

}