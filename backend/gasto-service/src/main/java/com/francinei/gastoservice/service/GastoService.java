package com.francinei.gastoservice.service;

import com.francinei.gastoservice.model.Gasto;

public interface GastoService {

	void salvarGasto(Gasto gasto);

	Iterable<Gasto> retornarGastos();

	Gasto retornarGastoPorCodigo(Long codigo);

}
