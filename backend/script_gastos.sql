CREATE DATABASE dbgasto
    WITH 
    OWNER = postgres
    ENCODING = 'UTF8'
    CONNECTION LIMIT = -1;
	
CREATE SCHEMA gasto
    AUTHORIZATION postgres;

COMMENT ON SCHEMA gasto
    IS 'Schema referente ao gerenciamento de gastos.';

CREATE TABLE gasto.TB_GASTO
(
    ID_GASTO SERIAL NOT NULL,
    DS_NOME_PESSOA CHARACTER VARYING(150) NOT NULL,
    DS_DESCRICAO CHARACTER VARYING(500) NOT NULL,
    DT_DATA_HORA TIMESTAMP(6) with time zone NOT NULL,
    VL_VALOR NUMERIC(11,2) NOT NULL,
    DS_TAG CHARACTER VARYING(200) NOT NULL,
    PRIMARY KEY (ID_GASTO)
);

ALTER TABLE gasto.TB_GASTO OWNER to postgres;

COMMENT ON TABLE gasto.TB_GASTO IS 'Tabela responsável por armazenar os gastos de uma pessoa.';
COMMENT ON COLUMN gasto.TB_GASTO.ID_GASTO IS 'Código sequencial de identificação única.';
COMMENT ON COLUMN gasto.TB_GASTO.DS_NOME_PESSOA IS 'Nome da pessoa responsável pelo gasto realizado.';
COMMENT ON COLUMN gasto.TB_GASTO.DS_DESCRICAO IS 'Descrição referente ao detalhamento do gasto realizado.';
COMMENT ON COLUMN gasto.TB_GASTO.DT_DATA_HORA IS 'Data/hora da realização do gasto.';
COMMENT ON COLUMN gasto.TB_GASTO.VL_VALOR IS 'Valor do gasto realizado.';
COMMENT ON COLUMN gasto.TB_GASTO.DS_TAG IS 'Etiqueta para classificação do gasto.';