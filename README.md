# Controle de Gastos

Projeto para controle de gastos pessoais.

## Informações

Projeto focado no desenvolvimento do desafio proposto. Não possuindo publicação em ambiente produtivo.

## Construído com

## Backend
* [Java 11]
* [Spring Boot]
* [Spring Cloud]
* [Spring Data JPA/Hibernate]

## Frontend
* [Angular 9]
* [Angular Material]

## Autor

* Francinei Marin (francineimarin@gmail.com)
